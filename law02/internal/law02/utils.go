package law02

import (
	"os"
)

func SetAccessAddress(newAccessAddress string) {
	accessAddress = newAccessAddress
}

func getEnvOrDefault(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

func GetObjectURL(key string) string {
	return accessAddress + key
}
