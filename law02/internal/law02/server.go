package law02

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var accessAddress string = "http://infralabs.cs.ui.ac.id:20173/"

func ServeHTTP(
	name, bindAddress string,
	patternHandlers map[string]func(http.ResponseWriter, *http.Request)) {
	server := mux.NewRouter()

	for pattern, handler := range patternHandlers {
		server.HandleFunc(pattern, handler)
	}

	log.Printf("========================= %s =========================\n", name)
	log.Printf("Server starting at %s\n", bindAddress)
	http.ListenAndServe(bindAddress, server)
}
