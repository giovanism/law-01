package law02

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const (
	ClientId         = "cho7dohKahCh5be1fohy0Miep4ooCh9eep7ohYuk"
	ClientSecret     = "Pae3eime7ooBoaGaiWae9Agh2iVaiphezahth4ze"
	PasswordGrant    = "password"
	OAuthTokenUrl    = "http://oauth.infralabs.cs.ui.ac.id/oauth/token/"
	OAuthResourceUrl = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource/"

	AuthorizationHeader = "Authorization"
)

var (
	insecureClient = newInsecureClient()
)

func Authenticate(r *http.Request) (*Resource, error) {
	var emptyBytes []byte
	emptyBuf := bytes.NewBuffer(emptyBytes)

	newR, err := http.NewRequest(http.MethodGet, OAuthResourceUrl, emptyBuf)
	if err != nil {
		log.Println(err.Error())
		panic(err)
	}

	authHeader := r.Header.Get(AuthorizationHeader)
	if authHeader != "" {
		newR.Header.Add(AuthorizationHeader, authHeader)

		response, err := insecureClient.Do(newR)
		if err != nil {
			log.Println(err.Error())
			panic(err)
		}

		var resource Resource
		json.NewDecoder(response.Body).Decode(&resource)

		if response.StatusCode != http.StatusOK {
			log.Printf("%v\n", response)
			return &resource, errors.New("Bad token")
		}

		return &resource, nil

	} else {
		return nil, errors.New("No token provided")
	}
}

func GetToken(username, password string) TokenResponse {
	form := newTokenURLFormdata(username, password)
	newR, err := http.NewRequest(http.MethodPost, OAuthTokenUrl,
		strings.NewReader(form.Encode()))

	if err != nil {
		log.Println(err.Error())
		panic(err)
	}

	response, err := insecureClient.Do(newR)
	if err != nil {
		log.Println(err.Error())
		panic(err)
	}

	var token TokenResponse
	json.NewDecoder(response.Body).Decode(&token)

	return token
}

func newTokenURLFormdata(username, password string) url.Values {
	return url.Values{
		"username":      []string{username},
		"password":      []string{password},
		"grant_type":    []string{PasswordGrant},
		"client_id":     []string{ClientId},
		"client_secret": []string{ClientSecret},
	}
}

func newInsecureClient() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	return &http.Client{Transport: tr}
}

type Credential struct {
	Username     string
	Password     string
	GrantType    string
	ClientId     string
	ClientSecret string
}

type TokenResponse struct {
	AccessToken  string  `json:"access_token"`
	ExpiresIn    int64   `json:"expires_in"`
	TokenType    string  `json:"token_type"`
	Scope        *string `json:"scope"`
	RefreshToken string  `json:"refresh_token"`
}

type Resource struct {
	AccessToken string  `json:"access_token"`
	ClientId    string  `json:"client_id"`
	UserId      string  `json:"user_id"`
	Expires     int64   `json:"expires"`
	Scope       *string `json:"scope"`
}
