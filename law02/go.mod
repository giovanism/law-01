module gitlab.com/giovanism/law-01/law02

go 1.12

require (
	github.com/aws/aws-sdk-go v1.29.16
	github.com/docker/docker v1.13.1 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/gorilla/mux v1.7.4
)
