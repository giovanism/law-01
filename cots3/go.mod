module gitlab.com/giovanism/law-01/cots3

go 1.12

require (
	github.com/gorilla/mux v1.7.4
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
)
