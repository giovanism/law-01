package main

import (
	"flag"

	internal "gitlab.com/giovanism/law-01/cots3/internal/cots3"
	"gitlab.com/giovanism/law-01/cots3/internal/cots3/cots3"
)

const defaultListenAddress = "127.0.0.1:8000"

func main() {
	bindAddress := flag.String("bind", defaultListenAddress, "Address to listen")

	flag.Parse()

	internal.ServeHTTP("cots3", *bindAddress, cots3.Handlers)
}
