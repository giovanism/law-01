package store

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/giovanism/law-01/law03/internal/law03"
)

var (
	Handlers  = newHandlers()
	indexTmpl = template.Must(template.ParseFiles("web/templates/index.html"))
)

func newHandlers() map[string]func(http.ResponseWriter, *http.Request) {
	return map[string]func(http.ResponseWriter, *http.Request){
		"/upload/": upload,
		"/":        index,
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	indexTmpl.Execute(w, nil)
}

func upload(w http.ResponseWriter, r *http.Request) {
	jsonEnc := json.NewEncoder(w)

	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				log.Printf("%v", err.Error())
				errData := map[string]string{
					"message": "Invalid Request",
					// "error": err.Error(),
				}

				w.WriteHeader(http.StatusBadRequest)
				jsonEnc.Encode(errData)
			}
		}
	}()

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
	} else {
		mulFile, _, err := r.FormFile("selectedFile")
		if err != nil {
			panic(err)
		}

		u, err := uuid.NewRandom()
		if err != nil {
			panic(err)
		}

		routingKey := u.String()
		compressResponse, err := law03.UploadToServer2(routingKey, mulFile)
		if err != nil {
			panic(err)
		}

		jsonEnc.Encode(compressResponse)
	}
}
