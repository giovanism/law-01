package law03

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

var accessAddress string = "http://infralabs.cs.ui.ac.id:20177/"

func getEnvOrDefault(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

func SetAccessAddress(newAccessAddress string) {
	accessAddress = newAccessAddress
}

func UploadToServer2(routingKey string, body io.Reader) (*CompressResponse, error) {
	b := new(bytes.Buffer)
	w := multipart.NewWriter(b)
	part, err := w.CreateFormFile("file", "file")
	if err != nil {
		return nil, err
	}
	buf, err := ioutil.ReadAll(body)
	if err != nil {
		return nil, err
	}
	part.Write(buf)
	// Dont defer
	w.Close()

	req, err := http.NewRequest(http.MethodPost, accessAddress+"compress/", b)
	if err != nil {
		return nil, err
	}

	req.Header.Add("X-ROUTING-KEY", routingKey)
	req.Header.Add("Content-Type", w.FormDataContentType())

	r, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	jsonDec := json.NewDecoder(r.Body)

	var response CompressResponse
	jsonDec.Decode(&response)

	return &response, err
}

type CompressResponse struct {
	Message    string `json:"message"`
	RoutingKey string `json:"routing_key"`
}
