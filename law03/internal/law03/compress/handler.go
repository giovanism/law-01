package compress

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var (
	Handlers   = newHandlers()
	tempDir, _ = ioutil.TempDir("", "server_2")
)

func newHandlers() map[string]func(http.ResponseWriter, *http.Request) {
	return map[string]func(http.ResponseWriter, *http.Request){
		"/compress/": compress,
		"/{key}":     index,
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	key := params["key"]

	http.Redirect(w, r, "http://infralabs.cs.ui.ac.id:20173/"+key, http.StatusMovedPermanently)
}

func compress(w http.ResponseWriter, r *http.Request) {
	jsonEnc := json.NewEncoder(w)

	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				log.Printf("%v", err.Error())
				errData := map[string]string{
					"message": "Invalid Request",
					// "error": err.Error(),
				}

				w.WriteHeader(http.StatusBadRequest)
				jsonEnc.Encode(errData)
			}
		}
	}()

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)

	} else {
		routingKey := r.Header.Get("X-ROUTING-KEY")
		if routingKey == "" {
			panic(errors.New("No X-ROUTING-KEY header present"))
		}

		mulFile, mulFileHeader, err := r.FormFile("file")
		if err != nil {
			panic(err)
		}

		toCompress, err := ioutil.ReadAll(mulFile)
		if err != nil {
			panic(err)
		}

		fileName := routingKey + ".zip"

		// Compressing part
		buf := new(bytes.Buffer)
		w := zip.NewWriter(buf)
		f, err := w.Create(mulFileHeader.Filename)
		if err != nil {
			panic(err)
		}
		_, err = f.Write(toCompress)
		if err != nil {
			panic(err)
		}
		err = w.Close()
		if err != nil {
			panic(err)
		}

		tmpFile, err := ioutil.TempFile(tempDir, "*-"+fileName)
		defer tmpFile.Close()
		_, err = tmpFile.Write(buf.Bytes())
		if err != nil {
			panic(err)
		}

		go DummyZipPublisher()

		saveData := map[string]string{
			"message":     "Saved",
			"routing_key": routingKey,
		}

		log.Printf("%v", saveData)
		jsonEnc.Encode(saveData)
	}
}
