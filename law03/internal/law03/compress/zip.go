package compress

import (
	"archive/zip"
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"time"
)

func Zip(filename string, reader io.Reader) io.Reader {
	buf := new(bytes.Buffer)
	w := zip.NewWriter(buf)

	fileBody, err := ioutil.ReadAll(reader)
	if err != nil {
		panic(err)
	}

	// Add some files to the archive.
	var files = []struct {
		Name string
		Body []byte
	}{
		{filename, fileBody},
	}

	for _, file := range files {
		f, err := w.Create(file.Name)
		if err != nil {
			log.Fatal(err)
		}
		_, err = f.Write([]byte(file.Body))
		if err != nil {
			log.Fatal(err)
		}
	}

	err = w.Close()
	if err != nil {
		panic(err)
	}

	return bytes.NewReader(buf.Bytes())
}

func DummyZipPublisher() {
	for i := 0; i < 10; i++ {
		// Publish to AMQP

		time.Sleep(2 * time.Second)
	}
}
