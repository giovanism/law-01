package main

import (
	"flag"

	law03 "gitlab.com/giovanism/law-01/law03/internal/law03"
	compress "gitlab.com/giovanism/law-01/law03/internal/law03/compress"
)

const (
	defaultBindAddress = "0.0.0.0:20177"
)

func main() {
	bindAddress := flag.String("bind", defaultBindAddress, "Address to listen")

	flag.Parse()
	law03.ServeHTTP("store", *bindAddress, compress.Handlers)
}
