package main

import (
	"flag"

	law03 "gitlab.com/giovanism/law-01/law03/internal/law03"
	store "gitlab.com/giovanism/law-01/law03/internal/law03/store"
)

const (
	defaultBindAddress   = "0.0.0.0:20176"
	defaultAccessAddress = "http://infralabs.cs.ui.ac.id:20176/"
)

func main() {
	accessAddress := flag.String("access", defaultAccessAddress, "Object URL address")
	bindAddress := flag.String("bind", defaultBindAddress, "Address to listen")

	flag.Parse()

	law03.SetAccessAddress(*accessAddress)

	law03.ServeHTTP("store", *bindAddress, store.Handlers)
}
