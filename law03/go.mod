module gitlab.com/giovanism/law-01/law03

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
)
