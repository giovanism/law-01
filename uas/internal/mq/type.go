package mq

type MqAccess struct {
	User  string
	Pass  string
	Host  string
	Port  string
	VHost string
}
