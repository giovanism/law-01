package mq

import (
	"log"

	"github.com/streadway/amqp"
	"gitlab.com/giovanism/law-01/uas/internal/common"
)

var (
	mqHost  = common.GetEnvOrDefault("MQ_HOST", "mq")
	mqUser  = common.GetEnvOrDefault("MQ_USER", "0806444524")
	mqPass  = common.GetEnvOrDefault("MQ_PASS", "0806444524")
	mqVHost = common.GetEnvOrDefault("MQ_VHOST", "/")
	mqPort  = common.GetEnvOrDefault("MQ_PORT", "5672")

	Access MqAccess = MqAccess{mqUser, mqPass, mqHost, mqPort, mqVHost}
)

const (
	// Direct exchange
	DirectExchange string = "amq.direct"
	// Topic exchange
	TopicExchange string = "amq.topic"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func Publish(exchange, topic, contentType, body string) {
	conn, ch := SetUp()
	defer CleanUp(conn, ch)

	err := ch.Publish(
		exchange,
		topic,
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: contentType,
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
}

func Consume(topic, contentType string) {
	conn, ch := SetUp()
	defer CleanUp(conn, ch)

	msgs, err := ch.Consume(
		topic,
		"",    // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func QueueDeclare(name string) {
	conn, ch := SetUp()
	defer CleanUp(conn, ch)

	_, err := ch.QueueDeclare(
		name,
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")
}

func SetUp() (*amqp.Connection, *amqp.Channel) {
	address := "amqp://" + mqUser + ":" + mqPass + "@" + mqHost + ":" + mqPort + mqVHost
	conn, err := amqp.Dial(address)
	failOnError(err, "Failed to connect to RabbitMQ")

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	return conn, ch
}

func CleanUp(conn *amqp.Connection, ch *amqp.Channel) {
	defer conn.Close()
	defer ch.Close()
}
