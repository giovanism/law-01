package s3

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/giovanism/law-01/uas/internal/common"
)

var (
	s3Host      = common.GetEnvOrDefault("S3_HOST", "127.0.0.1:9000")
	s3Bucket    = common.GetEnvOrDefault("S3_BUCKET", "uas")
	s3AccessKey = os.Getenv("S3_ACCESS_KEY")
	s3SecretKey = os.Getenv("S3_SECRET_KEY")

	Client = *newS3Client()
)

func newS3Client() *s3.S3 {
	if s3AccessKey == "" {
		log.Fatalln("")
	}

	// Configure to use MinIO Server
	s3Config := &aws.Config{
		Credentials:      credentials.NewStaticCredentials(s3AccessKey, s3SecretKey, ""),
		Endpoint:         aws.String(s3Host),
		Region:           aws.String("us-east-1"),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
	}

	newSession := session.New(s3Config)
	return s3.New(newSession)
}

func GetFile(key string) io.ReadCloser {
	inputGet := s3.GetObjectInput{
		Bucket: &s3Bucket,
		Key:    &key,
	}
	outputGet, err := Client.GetObject(&inputGet)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	return outputGet.Body
}

func PutFile(key, owner string, file io.Reader) {
	content, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	body := bytes.NewReader(content)

	inputPut := s3.PutObjectInput{
		Bucket: &s3Bucket,
		Key:    &key,
		Body:   body,
		Metadata: map[string]*string{
			"Owner": &owner,
		},
	}

	_, err = Client.PutObject(&inputPut)
	if err != nil {
		panic(err)
	}
}

func InitBucket() {
	_, err := Client.CreateBucket(&s3.CreateBucketInput{
		Bucket: &s3Bucket,
	})

	if err != nil {
		panic(err)
	}

	readOnlyAnonUserPolicy := map[string]interface{}{
		"Version": "2012-10-17",
		"Statement": []map[string]interface{}{
			{
				"Sid":       "AddPerm",
				"Effect":    "Allow",
				"Principal": "*",
				"Action": []string{
					"s3:GetObject",
				},
				"Resource": []string{
					fmt.Sprintf("arn:aws:s3:::%s/*", s3Bucket),
				},
			},
		},
	}

	policy, err := json.Marshal(readOnlyAnonUserPolicy)
	if err != nil {
		panic(err)
	}

	_, err = Client.PutBucketPolicy(&s3.PutBucketPolicyInput{
		Bucket: &s3Bucket,
		Policy: aws.String(string(policy)),
	})

	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
}
