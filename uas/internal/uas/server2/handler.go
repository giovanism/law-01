package server2

import (
	"bytes"
	"encoding/json"
	"errors"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"net/http"

	"gitlab.com/giovanism/law-01/uas/internal/mq"
)

var (
	Handlers map[string]func(http.ResponseWriter, *http.Request) = newHandlers()

	indexTmpl = template.Must(template.ParseFiles("web/templates/index.html"))

	errKey = errors.New("KeyError")
)

func newHandlers() map[string]func(http.ResponseWriter, *http.Request) {
	return map[string]func(http.ResponseWriter, *http.Request){
		"/download/": download,
		"/":          index,
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	indexTmpl.Execute(w, nil)
}

func download(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		jsonEnc := json.NewEncoder(w)

		var uploadFile io.ReadCloser
		var fileName string

		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					log.Printf("%v\n", err)
					errData := map[string]string{}
					if err == errKey {
						errData["msg"] = "Invalid Request"
						w.WriteHeader(http.StatusBadRequest)
					} else {
						errData["msg"] = "Server Error"
						w.WriteHeader(http.StatusInternalServerError)
					}

					jsonEnc.Encode(errData)
				}
			}
		}()

		log.Printf("%v", r)
		url := r.FormValue("url")
		if url == "" {
			panic(errKey)
		}

		response, err := http.Get(url)
		if err != nil {
			panic(err)
		}
		contentDisposition := response.Header.Get("Content-Disposition")
		_, params, _ := mime.ParseMediaType(contentDisposition)

		uploadFile = response.Body
		fileName, _ = params["filename"]

		if fileName == "" {
			fileName = "file" // Assign default filename
		}

		content, err := ioutil.ReadAll(uploadFile)
		if err != nil {
			panic(err)
		}

		bytes.NewReader(content) // TODO Read Later

		saveData := map[string]string{
			"msg":      "Saved",
			"filename": fileName,
		}

		log.Printf("%s\n", fileName)
		mq.Publish(fileName)
		jsonEnc.Encode(saveData)

	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
