package server1

import (
	"encoding/json"
	"errors"
	"html/template"
	"log"
	"net/http"

	"gitlab.com/giovanism/law-01/uas/internal/mq"
	"gitlab.com/giovanism/law-01/uas/internal/uas"
)

var (
	Handlers map[string]func(http.ResponseWriter, *http.Request) = newHandlers()

	indexTmpl = template.Must(template.ParseFiles("web/templates/index.html"))

	errKey = errors.New("KeyError")
)

func newHandlers() map[string]func(http.ResponseWriter, *http.Request) {
	return map[string]func(http.ResponseWriter, *http.Request){
		"/download/": download,
		"/":          index,
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	access := mq.Access
	access.Host = "localhost" // accessible to browser
	indexTmpl.Execute(w, access)
}

func download(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		jsonEnc := json.NewEncoder(w)

		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					log.Printf("%v\n", err)
					errData := map[string]string{}
					if err == errKey {
						errData["msg"] = "Invalid Request"
						w.WriteHeader(http.StatusBadRequest)
					} else {
						errData["msg"] = "Server Error"
						w.WriteHeader(http.StatusInternalServerError)
					}

					jsonEnc.Encode(errData)
				}
			}
		}()

		log.Printf("%v", r)
		err := r.ParseMultipartForm(2 << 20) // 2MB
		if err != nil {
			panic(err)
		}

		log.Printf("%v", r.PostForm)
		var job uas.DownloadJob
		if urls, ok := r.PostForm["url"]; ok {
			job = uas.DownloadJob{urls}
		}

		if len(job.Urls) > 10 {
			panic(errors.New("more than 10 urls"))
		}

		b, err := json.Marshal(job)

		mq.Publish(mq.DirectExchange, "server2", "application/json", string(b))
		jsonEnc.Encode(job)

	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
