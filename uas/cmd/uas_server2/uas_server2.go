package main

import (
	"log"

	"github.com/streadway/amqp"
	"gitlab.com/giovanism/law-01/uas/internal/mq"
)

func main() {
	log.Printf("========================= %s =========================\n", "uas_server2")
	mq.Consume("server2", "application/json")
}

func handler(msgs <-chan amqp.Delivery) {
	for d := range msgs {
		log.Printf("Received a message: %s", d.Body)
	}
}
