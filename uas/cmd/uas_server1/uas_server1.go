package main

import (
	"flag"

	common "gitlab.com/giovanism/law-01/uas/internal/common"
	"gitlab.com/giovanism/law-01/uas/internal/uas/server1"
)

const defaultListenAddress = "0.0.0.0:8000"

func main() {
	bindAddress := flag.String("bind", defaultListenAddress, "Address to listen")

	flag.Parse()

	common.ServeHTTP("uas_server1", *bindAddress, server1.Handlers)
}
