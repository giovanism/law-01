package main

import (
	"log"
	"time"

	"gitlab.com/giovanism/law-01/uas/internal/mq"
)

func main() {
	log.Printf("========================= %s =========================\n", "uas_server5")

	for {
		time.Sleep(time.Second)
		timeString := time.Now().Format(time.RFC1123Z)
		mq.Publish(mq.TopicExchange, "waktu", "text/plain", timeString)
		log.Printf("Time: %s", timeString)
	}
}
