ARG GO_BINARY=uas_server1
ARG PACKAGE_PATH=gitlab.com/giovanism/law-01/uas

FROM golang:alpine

ARG GO_BINARY
ARG PACKAGE_PATH

ADD . /go/src/$PACKAGE_PATH

WORKDIR /go/src/$PACKAGE_PATH
RUN go build cmd/$GO_BINARY/$GO_BINARY.go

FROM alpine

ARG GO_BINARY
ARG PACKAGE_PATH

COPY --from=0 /go/src/$PACKAGE_PATH/$GO_BINARY /go/bin/$GO_BINARY
ADD web/ /web/

ENV ABS_BINARY /go/bin/${GO_BINARY}

CMD $ABS_BINARY
