package main

import (
	"encoding/json"
	"flag"
	"html/template"
	"log"
	"net/http"
	"net/url"
)

const (
	defaultBindAddress = "127.0.0.1:8000"
)

var (
	indexTmpl = template.Must(template.ParseFiles("index.html"))
)

func main() {
	bindAddress := flag.String("bind", defaultBindAddress, "Address to listen")
	flag.Parse()

	log.Print("========================= LAW-01 =========================")
	log.Printf("Server started at %s\n", *bindAddress)

	serveHTTP(*bindAddress)
}

func index(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s\n", r.Method, r.URL)

	key := r.URL.Query().Get("q")

	pageData := PageData{
		PageTitle: "GitHub Repo Search with Owner Thumbnail",
		Key:       key,
	}

	if key != "" {
		query := url.Values{}
		query.Set("q", key)

		reqURL := url.URL{
			Scheme:   "https",
			Host:     "api.github.com",
			Path:     "/search/repositories",
			RawQuery: query.Encode(),
		}

		log.Printf("Request: %s\n", reqURL.String())

		resp, err := http.Get(reqURL.String())
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		var repoQuery RepoQuery
		json.NewDecoder(resp.Body).Decode(&repoQuery)

		pageData.RepoQuery = repoQuery
	}

	indexTmpl.Execute(w, pageData)
}

func serveHTTP(bindAddress string) {
	server := http.NewServeMux()
	server.HandleFunc("/", index)
	http.ListenAndServe(bindAddress, server)
}

type PageData struct {
	PageTitle string
	Key       string
	RepoQuery RepoQuery
}

type RepoQuery struct {
	TotalCount        int    `json:"total_count"`
	IncompleteResults bool   `json:"incomplete_results"`
	Items             []Repo `json:"items"`
}

type Repo struct {
	ID              int          `json:"id"`
	Name            string       `json:"name"`
	FullName        string       `json:"full_name"`
	Owner           Account      `json:"owner"`
	Private         bool         `json:"private"`
	HTMLURL         template.URL `json:"html_url"`
	Description     string       `json:"description"`
	URL             template.URL `json:"url"`
	Homepage        template.URL `json:"homepage"`
	StargazersCount int          `json:"stargazers_count"`
	Language        string       `json:"language"`
	ForksCount      int          `json:"forks_count"`
}

type Account struct {
	ID        int          `json:"id"`
	Login     string       `json:"login"`
	AvatarURL template.URL `json:"avatar_url"`
	URL       template.URL `json:"url"`
	HTMLURL   template.URL `json:"html_url"`
}
