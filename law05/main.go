package main

import (
	"encoding/json"
	"flag"
	"net"
	"net/http"
	"strconv"

	logrustash "github.com/bshuster-repo/logrus-logstash-hook"
	"github.com/sirupsen/logrus"
)

const (
	defaultBindAddress = "127.0.0.1:8000"
)

var (
	log = logrus.New()
)

func main() {
	bindAddress := flag.String("bind", defaultBindAddress, "Address to listen")
	endpoint := flag.String("endpoint", "", "TCP Logstash Endpoint to Log into")
	flag.Parse()

	if *endpoint == "" {
		log.Fatalln("No Stack Endpoint Provided!")
	}

	// TCP Logstash endpoint
	conn, err := net.Dial("tcp", *endpoint)
	if err != nil {
		log.Fatalln(err)
	}
	hook := logrustash.New(conn, logrustash.DefaultFormatter(logrus.Fields{"type": "myappName"}))

	log.Level = logrus.DebugLevel // Enable Debug Level Log
	log.Hooks.Add(hook)
	ctx := log.WithFields(logrus.Fields{
		"method": "main",
	})

	ctx.Infoln("========================= LAW-01 =========================")
	ctx.Infof("Server started at %s\n", *bindAddress)

	serveHTTP(*bindAddress)
}

func index(w http.ResponseWriter, r *http.Request) {
	ctx := log.WithFields(logrus.Fields{
		"method": "index",
	})

	ctx.Infof("%s %s\n", r.Method, r.URL)
	w.Header().Set("Content-Type", "application/json")
	jsonEnc := json.NewEncoder(w)

	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				ctx.Errorf("%v", err.Error())
				errData := map[string]string{
					"msg": "Invalid Request",
					// "error": err.Error(),
				}

				w.WriteHeader(http.StatusBadRequest)
				jsonEnc.Encode(errData)
			}
		}
	}()

	n, err := strconv.Atoi(r.URL.Query().Get("n"))
	if err != nil {
		panic(err)
	}

	fibbonaciSeq := fibbonaci(n)

	jsonEnc.Encode(fibbonaciSeq)
}

func serveHTTP(bindAddress string) {
	server := http.NewServeMux()
	server.HandleFunc("/", index)
	http.ListenAndServe(bindAddress, server)
}

func fibbonaci(n int) []int {
	ctx := log.WithFields(logrus.Fields{
		"method": "fibbonaci",
	})

	fibbonaciSeq := make([]int, 0, 20)

	if n > 0 {
		fibbonaciSeq = append(fibbonaciSeq, 0)
		ctx.Debugf("Fibbo: %v\n", fibbonaciSeq)

		if n > 1 {
			fibbonaciSeq = append(fibbonaciSeq, 1)
			ctx.Debugf("Fibbo: %v\n", fibbonaciSeq)

			if n > 2 {
				for i := 2; i < n; i++ {
					fibbonaciSeq = append(fibbonaciSeq, fibbonaciSeq[i-2]+fibbonaciSeq[i-1])
					ctx.Debugf("Fibbo: %v\n", fibbonaciSeq)
				}
			}
		}
	}

	return fibbonaciSeq
}
