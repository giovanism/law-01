package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

const (
	defaultListenAddress = "127.0.0.1:8000"
)

func main() {
	log.Print("========================= COTS-01 =========================")
	log.Printf("Server started at %s\n", defaultListenAddress)

	serveHTTP()
}

func index(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s\n", r.Method, r.URL)

	var (
		l   float64
		h   float64
		err error
	)

	l, err = strconv.ParseFloat(r.URL.Query().Get("l"), 64)
	if err != nil {
		panic(err)
	}

	h, err = strconv.ParseFloat(r.URL.Query().Get("h"), 64)
	if err != nil {
		panic(err)
	}

	arithmetic := NewArithmetic(l, h)

	body, err := json.Marshal(arithmetic)

	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

func serveHTTP() {
	server := http.NewServeMux()
	server.HandleFunc("/", index)
	http.ListenAndServe(defaultListenAddress, server)
}

type Arithmetic struct {
	Length        float64 `json:"length"`
	Height        float64 `json:"height"`
	RectangleArea float64 `json:"rectangle_area"`
	TriangleArea  float64 `json:"triangle_area"`
}

func NewArithmetic(length float64, height float64) Arithmetic {
	a := Arithmetic{
		Height:        height,
		Length:        length,
		RectangleArea: length * height,
		TriangleArea:  length * height / 2,
	}

	return a
}
